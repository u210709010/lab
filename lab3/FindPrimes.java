public class FindPrimes {
    public static void main(String[] args) {
        int max = Integer.parseInt(args[0]);

        for(int number = 2; number < max; ++number) {
            int divisor = 2;

            boolean isPrime;
            for(isPrime = true; divisor < number && isPrime; ++divisor) {
                if (number % divisor == 0) {
                    isPrime = false;
                }
            }

            if (isPrime) {
                System.out.println(number + " ");
            }
        }

    }
}