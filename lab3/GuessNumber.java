import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in); //Creates an object to read user input
        Random rand = new Random(); //Creates an object from Random class
        int number = rand.nextInt(100); //generates a number between 0 and 99


        System.out.println("Hi! I'm thinking of a number between 0 and 99.");
        System.out.print("Can you guess it: ");

        int guess = reader.nextInt(); //Read the user input
        int count = 1;

        while (guess != number && guess != -1) {
            System.out.println("Sorry, guess is not correct.");
            if (guess < number)
                System.out.println("Number is greater than your guess.");
            else
                System.out.println("Number is less than your guess.");

            System.out.println("If you want to exit, type -1 or try again: ");
            guess = reader.nextInt();
            count++;
        }

        if (guess == number)

            System.out.println("Congrulations, you won after " + count + "attempts.");
        else
            System.out.println("Sorry,you entered the wrong number, the number is  " + number);

        reader.close(); //Close the resource before exiting
    }
}