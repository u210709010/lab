public class Point {

    private final int xCoord;
    private final int yCoord;

    public Point(int xCoord, int yCoord){
        this.xCoord = xCoord;
        this.yCoord = yCoord;
    }

    public int getxCoord() {
        return xCoord;
    }

    public int getyCoord() {
        return yCoord;
    }

    public double distanceFromPoint(Point point){
        int xDiff = xCoord - point.xCoord;
        int yDiff = yCoord - point.yCoord;

        return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
    }

}
